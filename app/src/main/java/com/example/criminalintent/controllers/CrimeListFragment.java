package com.example.criminalintent.controllers;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.graphics.Color;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.criminalintent.R;
import com.example.criminalintent.models.Crime;
import com.example.criminalintent.models.CrimeLab;

import java.util.List;

public class CrimeListFragment extends Fragment {

    private RecyclerView mCrimeRecyclerView;
    private CrimeAdapter mCrimeAdapter;
    private Button mButtonAjouter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container , Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.crime_list_fragment, container, false);
        mCrimeRecyclerView = (RecyclerView) view.findViewById(R.id.crime_recycler_view);
        mButtonAjouter = (Button) view.findViewById(R.id.BoutonAjouter);

        mButtonAjouter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), CrimeAjouteActivity.class);
                startActivity(intent);
            }
        });


        mCrimeRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        updateUI();
        return view;
    }

    public void onResume(){
        super.onResume();
        updateUI();
    }

    private void updateUI() {
        CrimeLab crimeLab = CrimeLab.get(getActivity());
        List<Crime> crimes = crimeLab.getCrimes();

        if(mCrimeAdapter == null)
        {
            mCrimeAdapter = new CrimeAdapter(crimes);
            mCrimeRecyclerView.setAdapter(mCrimeAdapter);
        }else
        {
            mCrimeAdapter.setItems(crimes);
          mCrimeAdapter.notifyDataSetChanged();
        }
    }

    private class CrimeHolder extends RecyclerView.ViewHolder{

        private Crime mCrime;
        private TextView mTitleTextView;
        private  TextView mDateTextView;
        private LinearLayout mLinear;
        private  TextView mGravite;


        public CrimeHolder(final LayoutInflater inflater, ViewGroup parent)
        {
            super(inflater.inflate(R.layout.list_item_crime, parent, false));
            mTitleTextView = (TextView) itemView.findViewById(R.id.crime_title);
            mDateTextView = (TextView) itemView.findViewById(R.id.crime_date);
            mLinear = (LinearLayout) itemView.findViewById(R.id.Linear);
            mGravite = (TextView) itemView.findViewById(R.id.crime_gravite);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), CrimeActivity.class);
                    intent.putExtra(CrimeFragment.CRIME_ID, mCrime.getId());
                    startActivity(intent);
                }
            });

        }
        public void bind(Crime crime){
            mCrime = crime;
            mTitleTextView.setText(crime.getTitle());
            mDateTextView.setText(crime.getDate().toString());
            mGravite.setText(crime.getGravite());
            if(crime.getSolved())
            {
                mLinear.setBackgroundColor(Color.GREEN);
            }else
            {
                mLinear.setBackgroundColor(Color.RED);
            }
        }

    }

    private class CrimeAdapter extends RecyclerView.Adapter<CrimeHolder>{

        private List<Crime> mCrimes;

        public CrimeAdapter(List<Crime> crimes)
        {
            mCrimes = crimes;
        }

        @Override
        public CrimeHolder onCreateViewHolder(ViewGroup parent, int viewType){
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            return new CrimeHolder(layoutInflater,parent);
        }
        @Override
        public int getItemCount(){
            return mCrimes.size();
        }
        @Override
        public  void onBindViewHolder(CrimeHolder holder, int position){

            Crime crime = mCrimes.get(position);
            holder.bind(crime);
        }


        public void setItems(List<Crime> crimesliste) {
            this.mCrimes = crimesliste;
        }
    }

}


