package com.example.criminalintent.controllers;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.criminalintent.R;
import com.example.criminalintent.models.Crime;
import com.example.criminalintent.models.CrimeLab;

import java.util.Date;

public class CrimeAjoute extends Fragment implements AdapterView.OnItemSelectedListener{


    private TextView mTitleField;
    private Button mDatebutton;
    private CheckBox mSolvedCheckBox;
    public static final String CRIME_ID = "CRIME_ID";
    private Spinner mSpinnerGravite;
    private Date mDate = new Date();
    private Button mAjouteButton;
    private boolean resolue;
    private String gravite;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //mCrime= new Crime();


    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_crime_ajoute, container, false);

        mTitleField = (EditText) v.findViewById(R.id.crime_title);


        mDatebutton = (Button) v.findViewById(R.id.crime_date);
        mDatebutton.setText(mDate.toString());
        mDatebutton.setEnabled(false); //readonly


        mSolvedCheckBox = (CheckBox) v.findViewById(R.id.crime_solved);

        mSolvedCheckBox.setOnCheckedChangeListener(
                new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        resolue = isChecked;
                    }
                }
        );


        mSpinnerGravite = (Spinner) v.findViewById(R.id.gravité);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this.getContext(), R.array.gravité, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerGravite.setAdapter(adapter);

        mSpinnerGravite.setOnItemSelectedListener(this);

        mAjouteButton = (Button) v.findViewById(R.id.AjouteCrime);

        mAjouteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CrimeLab crimeLab = CrimeLab.get(getContext());
                Crime crime = new Crime();

                crime.setTitle(mTitleField.getText().toString());
                crime.setDate(new Date());
                crime.setSolved(resolue);
                crime.setGravite(gravite);

                crimeLab.addCrime(crime);
            }
        });

        return v;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String text = parent.getItemAtPosition(position).toString();
        Toast.makeText(parent.getContext(), text, Toast.LENGTH_SHORT);
        gravite = text;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
