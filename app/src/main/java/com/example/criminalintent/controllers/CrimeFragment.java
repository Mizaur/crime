package com.example.criminalintent.controllers;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.criminalintent.R;
import com.example.criminalintent.models.Crime;
import com.example.criminalintent.models.CrimeLab;

import java.util.UUID;

public class CrimeFragment extends Fragment implements AdapterView.OnItemSelectedListener {
    protected Crime mCrime;
    private TextView mTitleField;
    private Button mDatebutton;
    private CheckBox mSolvedCheckBox;
    public static final String CRIME_ID = "CRIME_ID";
    private Spinner mSpinnerGravite;
    private TextView mGravite;
    private CrimeLab crimeLab = CrimeLab.get(getContext());


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        //mCrime= new Crime();
        UUID crime_id = (UUID)
                getActivity().getIntent().getSerializableExtra("CRIME_ID");
        mCrime= CrimeLab.get(getActivity()).getCrime(crime_id);
    }

    @Override
    public void onPause(){
        super.onPause();
        crimeLab.updateCrime(mCrime);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View v = inflater.inflate(R.layout.fragment_crime,container,false);

        mTitleField=(EditText) v.findViewById(R.id.crime_title);
        mTitleField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mCrime.setTitle(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mTitleField.setText(mCrime.getTitle());
        mDatebutton = (Button) v.findViewById(R.id.crime_date);
        mDatebutton.setText(mCrime.getDate().toString());
        mDatebutton.setEnabled(false); //readonly
        mGravite = (TextView) v.findViewById(R.id.textView_gravite);
        mGravite.setText("Le Crime est : " + mCrime.getGravite());

        mSolvedCheckBox = (CheckBox) v.findViewById(R.id.crime_solved);
        mSolvedCheckBox.setChecked(mCrime.getSolved());
        mSolvedCheckBox.setOnCheckedChangeListener(
                new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        mCrime.setSolved(isChecked);
                    }
                }
        );


        mSpinnerGravite= (Spinner) v.findViewById(R.id.gravité);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this.getContext(), R.array.gravité, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerGravite.setAdapter(adapter);

        mSpinnerGravite.setOnItemSelectedListener(this);

        return v;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            String text = parent.getItemAtPosition(position).toString();
            Toast.makeText(parent.getContext(),text,Toast.LENGTH_SHORT);
            mCrime.setGravite(text);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


}
