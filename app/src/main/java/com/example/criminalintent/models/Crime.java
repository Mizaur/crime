package com.example.criminalintent.models;

import java.util.Date;
import java.util.UUID;

public class Crime {

    private UUID mId;
    private String mTitle;
    private Date mDate;
    private boolean mSolved;
    private String mGravite;

    public String getGravite() {
        return mGravite;
    }


    public void setGravite(String mGravite) {
        this.mGravite = mGravite;
    }



    public Crime(){
        this(UUID.randomUUID());
    }

    public Crime(UUID id) {
        this.mId = id ;
        this.mDate = new Date();
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public void setDate(Date mDate) {
        this.mDate = mDate;
    }

    public void setSolved(boolean mSolved) {
        this.mSolved = mSolved;
    }


    public UUID getId() {
        return mId;
    }

    public String getTitle() {
        return mTitle;
    }

    public Date getDate() {
        return mDate;
    }

    public boolean getSolved() {
        return mSolved;
    }
}
