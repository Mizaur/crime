package com.example.criminalintent.models;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class CrimeBaseHelper extends SQLiteOpenHelper {
    private static final int VERSION = 1;
    private static final String DATABASE_NAME= "crimeBase.db";
    public CrimeBaseHelper(Context context){
        super(context, DATABASE_NAME, null ,VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + CrimeDbSchema.CrimeTable.NAME + "("
        + "_id integer primary key autoincrement, "
        + CrimeDbSchema.CrimeTable.cols.UUID + ", " + CrimeDbSchema.CrimeTable.cols.TTTLE +", "
        + CrimeDbSchema.CrimeTable.cols.DATE +", " + CrimeDbSchema.CrimeTable.cols.SOLVED + ", "
        + CrimeDbSchema.CrimeTable.cols.GRAVITY + ")"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
