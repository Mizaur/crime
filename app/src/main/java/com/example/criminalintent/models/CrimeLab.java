package com.example.criminalintent.models;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class CrimeLab {

    private static CrimeLab sCrimeLab;
    private Context mContext;
    private SQLiteDatabase mDatabase;



    public static CrimeLab get(Context context){
        if(sCrimeLab == null){
            sCrimeLab= new CrimeLab(context);
        }
        return sCrimeLab;
    }
    private CrimeLab(Context context)
    {
        mContext = context.getApplicationContext();
        mDatabase = new CrimeBaseHelper(mContext).getWritableDatabase();
    }

    
   public void addCrime(Crime crime){
        mDatabase.insert(CrimeDbSchema.CrimeTable.NAME, null , getContentValues(crime));

   }

   public void updateCrime(Crime crime){
        String uuidString = crime.getId().toString();
       ContentValues values = getContentValues(crime);

       mDatabase.update(CrimeDbSchema.CrimeTable.NAME,values,
               CrimeDbSchema.CrimeTable.cols.UUID+ " = ?", new String[] {uuidString});
   }

   public Crime getCrime(UUID id){
        CrimeCursorWrapper cursor = queryCrimes(CrimeDbSchema.CrimeTable.cols.UUID + " = ?",
                new String[] {id.toString()}
                );
        try{
            if(cursor.getCount() == 0)
                return null;
            cursor.moveToFirst();
            return cursor.getCrime();
        }finally {
            cursor.close();
        }
   }


    public List<Crime> getCrimes() {
        ArrayList<Crime> crimes = new ArrayList<>();

        CrimeCursorWrapper cursor = queryCrimes(null, null);
        try {
            cursor.moveToFirst();
            while(!cursor.isAfterLast()) {
                crimes.add(cursor.getCrime());
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }
        Log.d("DEBUGSQL", "getCrimes: " + crimes.size());
        return crimes;
    }

    private ContentValues getContentValues(Crime crime) {
        ContentValues values = new ContentValues();
        values.put(CrimeDbSchema.CrimeTable.cols.UUID,
                crime.getId().toString());
        values.put(CrimeDbSchema.CrimeTable.cols.TTTLE, crime.getTitle());
        values.put(CrimeDbSchema.CrimeTable.cols.DATE,
                crime.getDate().getTime());
        values.put(CrimeDbSchema.CrimeTable.cols.SOLVED, crime.getSolved()?
                1: 0);
        values.put(CrimeDbSchema.CrimeTable.cols.GRAVITY,
                crime.getGravite());
        return values;
    }

    private CrimeCursorWrapper queryCrimes(String whereClause, String[]
            whereArgs) {
        return new CrimeCursorWrapper(mDatabase.query(
                CrimeDbSchema.CrimeTable.NAME,
                null, //all columns
                whereClause,
                whereArgs,
                null,null,null));
    }
}


